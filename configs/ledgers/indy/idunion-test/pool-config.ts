import { IndyVdrPoolConfig } from '@aries-framework/indy-vdr';

import genesisFile from './genesis-file';

const config: IndyVdrPoolConfig = {
  indyNamespace: 'idunion:test',
  genesisTransactions: genesisFile,
  isProduction: false,
  connectOnStartup: true,
};

export default config;
